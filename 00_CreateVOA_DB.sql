/*
	00_CreateVOA_DB.SQL
	===================
    Creates an empty VOA database based on the documented specification.
*/
DROP DATABASE IF EXISTS VOA;

CREATE DATABASE IF NOT EXISTS VOA COLLATE utf8_unicode_ci;


