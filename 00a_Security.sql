/*
	00a_Security.sql
    ================
    Grants access to the VOA database for the root user from any host.
    
    REMARKS
    =======
    Ideally the password would not be contained within this file, but held in an encrypted
    form and injected in without exposing it to any but the appropriate mechanical device
*/
GRANT ALL PRIVILEGES ON VOA.* TO 'root'@'%' IDENTIFIED BY 'W@ll4c3';
GRANT FILE ON *.* TO 'root'@'%';
FLUSH PRIVILEGES;
