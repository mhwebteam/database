# VOA Database #

## What is this repository for? ##

To contain the SQL code to carry out the following tasks:-

 * Build the VOA database
 * Build the tables necessary to receive the VOA data from file
 * Load the data from file into the tables.

## Notes on the VOA data ##

 * Fields delimited by an asterix *
 * Records terminated by carriage return and line feed \r\n.  
 * The SMV data is supplied in a hierarchal file format with no intrinsic key for the descendent records.

## Notes on the database itself ##

### Schema diagram

![IMAGE](images/VOA_Data.png)

### Metadata###
All tables and fields are commented.  These comments will show up in an IDE and INFORMATION_SCHEMA views.

There are approximately 

 * 2 million records in the current VOA list
 * 1 million records in the historic VOA change list
 * 1 million records in the summary value (SMV) file

Approximate DB size <1GB

## SMV data
The SMV data is supplied in a single file but contains 7 separate record structures.  Each record is prefixed by the record type

 * 01 = The listing
 * 02 = The Line Items
 * 03 = Any additional items
 * 04 = Car Parking
 * 05 = Plant & Machinery
 * 06 = Adjustments
 * 07 = Adjustment Totals

This is further complicated by their being no explicit linking key in the different record types.  The approach to reformatting the data is to do the following:-

 * Read through the file line by line
 * When an 01 record is detected grab the Assessment Reference Number (unique key) into an id variable
 * Regardless of the record type print the current value of the id variable and the record.  In effect every record gains the Assessment Reference Number which can be used as a linking key.

This can be done in two stages using the AWK command:-
```
#!bash

# The split is run as two separate AWK commands because piping the 
# two together produces a "Too many files open" error.

awk -F* 'BEGIN{ OFS="*";}{if($1 == "01") a= $2;print a,$0}' SMV_2010_MERGED.dta.27May2015 >SMV_Reformat.dat
awk -F* '{print >$2}' SMV_Reformat.dat
```

### Database configuration ###

 * Collation = utf8_unicode_ci (case insensitive)
 * Engine = InnoDB

### Database Tables

There are 9 pairs of tables.  Each pair will consist of a STAGING table and a final counterpart.

The reason for the STAGING table is that the MySQL LOAD DATA INFILE statement is not reliable when asked to handle non-text data types, particularly dates and when a field is adjacent to a NULL value.

Tables are as follows:-

Loading table    | Final table   | Description
---------------- | ------------- |--------------
VOA_LIST_STAGING | VOA_LIST      | Current listing
VOA_HIST_STAGING | VOA_HIST      | Historic changes
VOA_SMV_01_STAGING  | VOA_SMV_01 | Rating List Details
VOA_SMV_02_STAGING  | VOA_SMV_02 | Line Items
VOA_SMV_03_STAGING  | VOA_SMV_03 | Additional Items
VOA_SMV_04_STAGING  | VOA_SMV_04 | Plant & Machinery
VOA_SMV_05_STAGING  | VOA_SMV_05 | Car Parking
VOA_SMV_06_STAGING  | VOA_SMV_06 | Adjustments
VOA_SMV_07_STAGING  | VOA_SMV_07 | Adjustment Totals