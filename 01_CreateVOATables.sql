/*
	01_CreateVOATables.SQL
    ======================
    Drops and recreates the following
    *.	8 tables in order to receive the VOA data
    *.	8 tables to act as the final resting place for the VOA data after all data type conversion issues have been resolved.
    
    The tables will have the same prefix.  The staging tables will have the suffix _STAGING
    1. VOA_HIST		- The Historic VOA data
    2. VOA_LIST 	- The Current VOA data
    3. VOA_SMV_02	- Summary Value for record type 02 (Line Items)
    4. VOA_SMV_03	- Summary value for record type 03 (Addtional items)
    5. VOA_SMV_04	- Summary value for record type 04 (Plant & Machinery)
    6. VOA_SMV_05	- Summary value for record type 05 (Car Parking)
    7. VOA_SMV_06	- Summary value for record type 06 (Adjustments)
    8. VOA_SMV_07	- Summary value for record type 07 (Adjustment totals)
    
    REMARKS
    =======
    The VOA_SMV data is concatenated into a single file although each record type has a separate structure.
    
    
*/

Use VOA;

DROP TABLE IF EXISTS VOA_HIST;
DROP TABLE IF EXISTS VOA_LIST;
DROP TABLE IF EXISTS VOA_SMV_01;
DROP TABLE IF EXISTS VOA_SMV_02;
DROP TABLE IF EXISTS VOA_SMV_03;
DROP TABLE IF EXISTS VOA_SMV_04;
DROP TABLE IF EXISTS VOA_SMV_05;
DROP TABLE IF EXISTS VOA_SMV_06;
DROP TABLE IF EXISTS VOA_SMV_07;
DROP TABLE IF EXISTS VOA_HIST_STAGING;
DROP TABLE IF EXISTS VOA_LIST_STAGING;
DROP TABLE IF EXISTS VOA_SMV_01_STAGING;
DROP TABLE IF EXISTS VOA_SMV_02_STAGING;
DROP TABLE IF EXISTS VOA_SMV_03_STAGING;
DROP TABLE IF EXISTS VOA_SMV_04_STAGING;
DROP TABLE IF EXISTS VOA_SMV_05_STAGING;
DROP TABLE IF EXISTS VOA_SMV_06_STAGING;
DROP TABLE IF EXISTS VOA_SMV_07_STAGING;

CREATE TABLE VOA_HIST_STAGING (
	BillingAuthorityCode CHAR(4) NOT NULL 
		COMMENT "Unique 4 digit code that is unique to the Billing Authority.  Can contain leading zeros.",
    CommunityCode VARCHAR(6) NULL 
		COMMENT "Where present this signifies the community within the Billing Authority",
    BAReferenceNumber VARCHAR(25) NOT NULL 
		COMMENT "Reference number for the Billing Authority",
    DescriptionCode VARCHAR(5) NOT NULL 
		COMMENT "Code refering to the description.",
    DescriptionText VARCHAR(60) NOT NULL 
		COMMENT "Usually relates to the description code but can be over-ridden",
    AddressKey BIGINT NOT NULL  
		COMMENT "Can be used to join addresses between the current and historic list",
    EffectiveDate VARCHAR(11) NULL  
		COMMENT "Will be NULL if the entry has never been altered or is the deleted part of a reconsitution",
    CompositeIndicator CHAR(1) NULL  
		COMMENT "Contains C if the property is used for both domestic and non-domestic purposes.",
    RateableValue BIGINT NULL  
		COMMENT "NULL indicates a deleted assessment",
    SettlementType CHAR(2) NULL  
		COMMENT "Indicates the method of settlement of a proposal/appeal made by an interested party",
    AssessmentReference BIGINT NULL  
		COMMENT "Unique key for an assessment irrespective of the status or list year.",
    ListAlterationDate VARCHAR(11) NULL  
		COMMENT "Creation or amendment date.",
    SpecialCategoryCode CHAR(4) NULL
		COMMENT "A code used by the VOA to group properties for operational purposes."
) COMMENT= 'Used to load the VOA Historic List.  The STAGING table is to handle data type upload problems.';

CREATE TABLE VOA_LIST_STAGING (
	VOA_LIST_ID BIGINT NOT NULL
		COMMENT "Auto-incrementing number within the file",
	BillingAuthorityCode CHAR(4) NOT NULL 
		COMMENT "Unique 4 digit code that is unique to the Billing Authority.  Can contain leading zeros.",
    CommunityCode VARCHAR(6) NULL 
		COMMENT "Where present this signifies the community within the Billing Authority",
    BAReferenceNumber VARCHAR(25) NOT NULL 
		COMMENT "Reference number for the Billing Authority",
    DescriptionCode VARCHAR(5) NOT NULL 
		COMMENT "Code refering to the description.",
    DescriptionText VARCHAR(60) NOT NULL 
		COMMENT "Usually relates to the description code but can be over-ridden",
    AddressKey BIGINT NOT NULL  
		COMMENT "Can be used to join addresses between the current and historic list",
	FullAddress VARCHAR(190) NOT NULL 
		COMMENT "Address as displayed in the ratings list",
	FirmName VARCHAR(50) NULL
		COMMENT "The name of the organisation at the premise",
	AddressNumberName VARCHAR(76) NULL
		COMMENT "The premise name or number.  Optional field",
	Street VARCHAR(36) NULL
		COMMENT "The main street address.  Note that sub-streets are not stored in this field",
	Town VARCHAR(36) NULL
		COMMENT "Possibly the post town but could equally be the locality",
	PostalDistrict VARCHAR(36) NULL 
		COMMENT "Most likely to be the post town if present.",
    County VARCHAR(36) NULL
		COMMENT "Optional as county is not required for postal delivery",
	PostCode VARCHAR(8) NULL
		COMMENT "Geographic postcode",
    EffectiveDate VARCHAR(11) NULL  
		COMMENT "Will be NULL if the entry has never been altered or is the deleted part of a reconsitution",
    CompositeIndicator CHAR(1) NULL  
		COMMENT "Contains C if the property is used for both domestic and non-domestic purposes.",
    RateableValue BIGINT NULL  
		COMMENT "NULL indicates a deleted assessment",
    SettlementType CHAR(2) NULL  
		COMMENT "Indicates the method of settlement of a proposal/appeal made by an interested party",
    AssessmentReference BIGINT NULL  
		COMMENT "Unique key for an assessment irrespective of the status or list year.",
    ListAlterationDate VARCHAR(11) NULL  
		COMMENT "Creation or amendment date.",
    SpecialCategoryCode CHAR(4) NULL
		COMMENT "A code used by the VOA to group properties for operational purposes.",
	SubStreetLevel3 VARCHAR(36) NULL
		COMMENT "Lowest level of any sub-division of street",
	SubStreetLevel2 VARCHAR(36) NULL
		COMMENT "Middle level of any sub-division of street",
	SubStreetLevel1 VARCHAR(36) NULL
		COMMENT "First level of subdivision"
) COMMENT= 'Used to load the VOA Current List.  The STAGING table exists to handle any data type upload problems.';



CREATE TABLE VOA_SMV_01_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for record type 01 is a copy of the AssessmentReference field.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 01 for Rating List Details",
    AssessmentReference BIGINT NULL  
		COMMENT "Unique key for an assessment irrespective of the status or list year.",
	AddressKey BIGINT NOT NULL  
		COMMENT "Can be used to join addresses between the current and historic list",
	BillingAuthorityCode CHAR(4) NOT NULL 
		COMMENT "Unique 4 digit code that is unique to the Billing Authority.  Can contain leading zeros.",
	FirmName VARCHAR(50) NULL
		COMMENT "The name of the organisation at the premise",
	AddressNumberName VARCHAR(76) NULL
		COMMENT "The premise name or number.  Optional field",
	SubStreetLevel3 VARCHAR(36) NULL
		COMMENT "Lowest level of any sub-division of street",
	SubStreetLevel2 VARCHAR(36) NULL
		COMMENT "Middle level of any sub-division of street",
	SubStreetLevel1 VARCHAR(36) NULL
		COMMENT "First level of subdivision",
	Street VARCHAR(36) NULL
		COMMENT "The main street address.  Note that sub-streets are not stored in this field",
	PostalDistrict VARCHAR(36) NULL 
		COMMENT "Most likely to be the post town if present.",
	Town VARCHAR(36) NULL
		COMMENT "Possibly the post town but could equally be the locality",
    County VARCHAR(36) NULL
		COMMENT "Optional as county is not required for postal delivery",
	PostCode VARCHAR(8) NULL
		COMMENT "Geographic postcode",
	SchemeReference BIGINT NULL
		COMMENT "Identifies the valuation scheme used to value the property",
    DescriptionText VARCHAR(60) NOT NULL 
		COMMENT "Usually relates to the description code but can be over-ridden",
	TotalArea DECIMAL(12,2) NULL
		COMMENT "The area of the line items for the property",
	Subtotal INT NULL
		COMMENT "The value of the line items for the property",
	TotalValue INT NULL
		COMMENT "Total Calculated value of the property",
	AdoptedRV	INT NULL
		COMMENT "The rateable value of the property as shown in the rating list",
	ListYear SMALLINT NULL
		COMMENT "The year of the rating list",
	BillingAuthority VARCHAR(40) NULL
		COMMENT "Name of the local authority that calculates the rates bill for each rateable property",        
    BAReferenceNumber VARCHAR(25) NOT NULL 
		COMMENT "Reference number for the Billing Authority",
	VORef BIGINT NULL
		COMMENT "Reference number for internal VOA use",
    FromDate CHAR(11) NULL
		COMMENT "Date that the valuation took affect",
    ToDate CHAR(11) NULL
		COMMENT "Final date that the valuation was in force",
    SpecialCategoryCode CHAR(4) NULL
		COMMENT "A code used by the VOA to group properties for operational purposes.",
	UnitOfMeasure VARCHAR(8) NULL
		COMMENT "Indicator to show the measuring standard used.  GIA = Gross Internal Area, NIA = Net Internal Area",
	UnadjustedPrice DECIMAL(8,2) NULL
		COMMENT "Unadjusted matrix price (£/M squared) for the primary survey unit"
) COMMENT= 'Summary Values, Rating List Details for Record Type 01'; 


CREATE TABLE VOA_SMV_02_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for record type 02 is the AssessmentReference field from the precending record type 01 entry.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 02 for Line Items",
	LineItemOrder	SMALLINT NOT NULL
		COMMENT "Order of the line items",
	FloorDescription VARCHAR(50) NOT NULL
		COMMENT "Description of the floor level i.e. Ground, First & Mezzanine",
	LineItemArea	DECIMAL(8,2) NOT NULL
		COMMENT "Area of the line item",
	LineItemPrice DECIMAL(8,2) NOT NULL
		COMMENT "Price applied to the line item",
	LineItemValue INT NOT NULL
		COMMENT "Value of the line item"
) COMMENT= 'Summary Values for Record Type 02 - Line Items';
        
CREATE TABLE VOA_SMV_03_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for this record type is the AssessmentReference field from the precending record type 01 entry.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 03 for Additional Items",
	OtherAdditionalDescription VARCHAR(240) NULL
		COMMENT "Any additional items to the property not included in the line items such as fron/rear vehicle display spaces",
	OtherAdditionalSize DECIMAL(12,1) NULL
		COMMENT "Area of the addition",
	OtherAddtionalPrice DECIMAL(8,2) NULL
		COMMENT "Price applied to the other addition",
	OtherAdditionalvalue INT NULL
		COMMENT "The value of the addition"
) COMMENT= 'Summary Values for Record Type 03 - Additional Items';

CREATE TABLE VOA_SMV_04_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for this record type is the AssessmentReference field from the precending record type 01 entry.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 04 for Plant & Machinery",
	PlantAndMachineryValue VARCHAR(10) NOT NULL
		COMMENT "The value of the plant and machinery"
) COMMENT= 'Summary Values for Record Type 04 - Plant & Machinery';

CREATE TABLE VOA_SMV_05_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for this record type is the AssessmentReference field from the precending record type 01 entry.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 05 for Car Parking",
	CarParkingSpaces INT NULL
		COMMENT "The number of car parking spaces",
	CarParkingSpaceValue INT NULL
		COMMENT "The value of the car parking spaces",
    CarParkingArea INT NULL
		COMMENT "The amount of area of car parking",
    CarParkingAreaValue INT NULL
		COMMENT "The value of the car parking area",
    CarParkingTotalValue INT NULL
		COMMENT "The total value for car parking included in the valuation."
) COMMENT= 'Summary Values for Record Type 05 - Car Parking';

CREATE TABLE VOA_SMV_06_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for this record type is the AssessmentReference field from the precending record type 01 entry.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 06 for Adjustments",
	AdjustmentDescription VARCHAR(51) NULL
		COMMENT "Description of any adjustments made",
	AdjustmentPercent DECIMAL(10,2) NULL
		COMMENT "Total value of the adjustments made.  Can be positive or negative."
) COMMENT= 'Summary Values for Record Type 06 - Adjustments';

CREATE TABLE VOA_SMV_07_STAGING (
    AssessmentReferenceKey BIGINT NULL  
		COMMENT "Unique key appended by Movehut which for this record type is the AssessmentReference field from the precending record type 01 entry.",
	RecordType CHAR(2) NOT NULL
		COMMENT "Zero padded 2 character code.  In this case 07 for Adjustment Totals",
	TotalBeforeAdjustment INT NULL
		COMMENT "Total Value Before Adjustments",
	TotalAdjustments INT NULL
		COMMENT "Total value of adjustments made.  Can be positive or negative."
) COMMENT= 'Summary Values for Record Type 07 - Adjustment Totals';
