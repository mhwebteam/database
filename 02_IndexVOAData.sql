/*
	02_IndexVOAData.sql
    ===================
    The VOA data as supplied is hierarchal and not relational.
    
    There is no `key` for additional items.  It is quite possible and legitimate to have duplicate records.
    The indexes below are created after profiling the data.  It is possible that the implied relationships
    are only valid for the current data supply and that the true relationship is slightly different.
    
*/
ALTER TABLE VOA.VOA_SMV_01_STAGING ADD CONSTRAINT PK_ PRIMARY KEY (AssessmentReferenceKey);
ALTER TABLE VOA.VOA_SMV_02_STAGING ADD CONSTRAINT PK_ PRIMARY KEY (AssessmentReferenceKey,LineItemOrder);
ALTER TABLE VOA.VOA_SMV_04_STAGING ADD CONSTRAINT PK_ PRIMARY KEY (AssessmentReferenceKey);
ALTER TABLE VOA.VOA_SMV_05_STAGING ADD CONSTRAINT PK_ PRIMARY KEY (AssessmentReferenceKey);
ALTER TABLE VOA.VOA_SMV_06_STAGING ADD CONSTRAINT PK_ PRIMARY KEY (AssessmentReferenceKey,AdjustmentDescription);
ALTER TABLE VOA.VOA_SMV_07_STAGING ADD CONSTRAINT PK_ PRIMARY KEY (AssessmentReferenceKey);

CREATE INDEX idx_VOA_SMV_03_STAGING ON VOA.VOA_SMV_03_STAGING(AssessmentReferenceKey,OtherAdditionalDescription);

